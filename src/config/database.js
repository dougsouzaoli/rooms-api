const { Pool } = require('pg'); 
const dotenv = require('dotenv'); 

dotenv.config(); 

// ==> Conexão com a o banco de dados: 
const pool = new Pool({ 
  connectionString: process.env.DATABASE_URL 
});

module.exports = { 
  query: (texto, params) => pool.query(texto, params), 
};