/**
 * File: server.js
 * Description: arquivo responsável por toda a configuração e execução da aplicação.
 * Data: 29/01/2022
 * Author: Douglas Oliveira
 */

const app = require('./src/app');

const port = process.env.PORT || 3000;
 
app.listen(port, () => {
  console.log('Aplicação executando na porta ', port);
});
