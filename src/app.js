const express = require('express');
const cors = require('cors');
const app = express();

// ==> Rotas da API:
const index = require('./routes/index');
const roomRoutes = require('./routes/room.routes');
const scheduleRoutes = require('./routes/schedule.routes');
const authRoutes = require('./routes/auth.routes');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.json({ type: 'application/vnd.api+json' }));
app.use(cors());

app.use(index);
app.use('/login/', authRoutes);
app.use('/rooms/', roomRoutes);
app.use('/schedules/', scheduleRoutes);

module.exports = app;