const router = require('express-promise-router')();
const scheduleController = require('../controllers/schedule.controller');

// => Retorna todas as agendas
router.get('/', scheduleController.getSchedules);

// => Cria uma nova agenda
router.post('/', scheduleController.createSchedule);

module.exports = router;
