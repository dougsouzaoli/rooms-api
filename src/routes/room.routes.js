const router = require('express-promise-router')(); 
const roomController = require('../controllers/room.controller'); 

// ==> Retorna uma lista de salas disponíveis em uma data específica
router.get('/available', roomController.getRoomsAvailable); 

// ==> Retorna uma sala específica
router.get('/:id', roomController.getRoomById); 

module.exports = router;
