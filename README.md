# Como rodar a aplicação

1. Faça a instalação dos projeto:
```bash
npm install
```
2. Crie o arquivo .env e adicione a seguinte linha:
`DATABASE_URL=postgres://thhsghvs:i-ShbmSA89I4AT_QNaYRUvADzoIYOGK-@castor.db.elephantsql.com/thhsghvs`

3. Para rodar a aplicação, basta executar o comando abaixo:
```bash
nodemon
```

Desta forma, já será possível executar os requests!


# Visão Geral

- A aplicação foi criada seguindo uma estrutura básica conforme os requisitos do desafio. Optei por utilizar algumas boas práticas que vejo como coisas mínimas fundamentais para uma aplicação de backend, são elas:

## DOTENV
De grande importância em qualquer aplicação para que dados confidenciais não sejam expostos.

## NODEMON
Também uma biblioteca bastante agregadora, que traz o benefício de atualizar aplicação sem precisar reinicia-la.

## JSONWEBTOKEN
Utilizada para criação de checagem do token jwt, para manter nossa aplicação restrita e controlada com um token de autenticação.

## POSTGRESSQL
Foi utilizado o banco de dados recomendado no desafio, o qual eu nunca havia usado, mas que encontrei bastante facilidade por ser SQL simples.

## ENTRE OUTRAS
Além dessas principais, foram utilizadas outras aplicações mais básicas do NODE, e que também agregam na construção e no desempenho.
