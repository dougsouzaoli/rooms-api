const db = require("../config/database"); 
const jwt = require('jsonwebtoken');

const checkToken = (req, res) => {
  const { token } = req.headers;
  jwt.verify(token, 'shhhhh', (err, decoded) => {
    if (err) res.status(401).send({ message: "Token inválido!" });
  });
}

const login = async (req, res) => { 
  if (req.body.email && req.body.password) {
    const email = req.body.email;
    const password = req.body.password;
    const user = await db.query('SELECT * FROM users WHERE email = $1 and password = $2', [email, password]);
    if (user) {
      const token = jwt.sign({ foo: 'bar' }, 'shhhhh');;
      res.json({token: token});
    } else {
      res.sendStatus(401);
    }
  } else {
    res.sendStatus(401);
  }
};

module.exports = {
  checkToken,
  login,
}
