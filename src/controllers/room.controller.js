const db = require("../config/database"); 
const auth = require('./auth.controller');

const getRoomById = async (req, res) => { 
  const roomId = parseInt(req.params.id);
  auth.checkToken(req, res);
  const { rows } = await db.query( 
    'SELECT * FROM rooms WHERE id = $1', [roomId], 
  ); 
  res.status(200).send(rows);
};

const getRoomsAvailable = async (req, res) => {
  auth.checkToken(req, res);
  const date = req.query.date;
  const { rows } = await db.query( 
    'SELECT * ' +
    'FROM rooms r ' +
    'INNER JOIN schedules s ' +
    'ON s.date = $1 ' +
    'AND s.status = $2', [date, 'available'], 
  ); 
  res.status(200).send(rows);
};

module.exports = {
  getRoomById,
  getRoomsAvailable,
}