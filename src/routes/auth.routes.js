const router = require('express-promise-router')(); 
const authController = require('../controllers/auth.controller'); 

// ==> Retorna um token de autenticação
router.post('/', authController.login); 

module.exports = router;
