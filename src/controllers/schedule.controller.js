const db = require("../config/database");
const auth = require('./auth.controller');

const checkRoomAvailable = async (req, res) => {
  const { room_id, date, period } = req.body;
  const { rows } = await db.query(
    'SELECT * ' +
    'FROM schedules ' +
    'WHERE room_id = $1 ' + 
    'AND date = $2 ' +
    'AND period = $3', [room_id, date, period]
  )
  if (rows.length === 0) {
    res.status(404).send({
      message: "Agenda não encontrada!"
    })
  } else {
    if (rows[0].status !== 'available') {
      res.status(401).send({
        message: "Agenda não disponível!"
      })
    }
  }
}

const getSchedules = async (req, res) => {
  auth.checkToken(req, res);
  const { rows } = await db.query( 
    'SELECT * FROM schedules'
  ); 
  res.status(200).send(rows);
}

const createSchedule = async (req, res) => {
  const { room_id, date, period } = req.body;
  auth.checkToken(req, res);
  await checkRoomAvailable(req, res);
  await db.query(
    'UPDATE schedules ' +
    'SET status = $1 ' +
    'WHERE room_id = $2 ' + 
    'AND date = $3 ' +
    'AND period = $4', ['reserved', room_id, date, period]
  )
  res.status(201).send({
    message: "Agenda adicionada com sucesso!",
    body: {
      schedule: { room_id, date, period }
    },
  });
};

module.exports = {
  getSchedules,
  createSchedule,
}